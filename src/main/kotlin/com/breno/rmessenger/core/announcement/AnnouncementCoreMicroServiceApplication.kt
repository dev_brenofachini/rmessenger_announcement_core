package com.breno.rmessenger.core.announcement

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AnnouncementCoreMicroServiceApplication

fun main(args: Array<String>) {
	runApplication<AnnouncementCoreMicroServiceApplication>(*args)
}
